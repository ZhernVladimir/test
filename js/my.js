let Preloader = function (selector) {
    this.selector = selector;

    this.show = function () {
        $(selector).removeClass('hide');
    }
    this.hide = function () {
        $(selector).addClass('hide');
    }
}

preloader = new Preloader(preloaderSelector);

let UserData = function (count) {
    this.count = count;
    this.search = '';
    this.data = [];
    this._data = [];
    this.load = function () {
        preloader.show();
        $.ajax({    
            url: 'https://randomuser.me/api/',
            data: {results: this.count},
            context: this
        }).done(function (data) {
            this.data  = [...data.results];
            this._data =  [...data.results];

            setTimeout(function () {
                $(this.app.app_selector + ' ' + this.app.selectors.filter).removeClass('hide');
                app.renderContent();
                app.renderStatistic();
                preloader.hide();
            }, 1000)
        });
    }


    this.statistic = function () {
        result = {
            users: 0,
            women: 0,
            men: 0,
            gender_by_qty: 'поровну',
            statistic: {},
            telegram: 0
        }
        result.users = this.data.length;
        for (elem in this.data) {
            if (this.data[elem].gender == 'male') {
                result.women++;
            } else {
                result.men++;
            }

            result.statistic[this.data[elem].nat] = (typeof result.statistic[this.data[elem].nat] == 'undefined') ? 1 : (result.statistic[this.data[elem].nat] + 1);
        }

        if (result.men > result.women) {
            result.gender_by_qty = 'Женщин';
        }

        if (result.women > result.men) {
            result.gender_by_qty = 'Мужчин';
        }

        return result;
    }

    this.compare =  {
        alphabet_desc: false,
        alphabet_asc: false,
        gender_m: false,
        gender_w: false,
    }

    this.comparison_methods = {
        alphabet_desc: function(a, b){
            return a.name.first.localeCompare(b.name.first);
        },
        alphabet_asc: function(b, a){
            return a.name.first.localeCompare(b.name.first);
        },
        gender_w: function(a, b){
            if(b.gender == 'male'  && a.gender == 'female') return 1;
            if(b.gender == 'female'  && a.gender == 'male') return -1;
            return 0;
        },
        gender_m: function(b, a){
            if(b.gender == 'male'  && a.gender == 'female') return 1;
            if(b.gender == 'female'  && a.gender == 'male') return -1;
            return 0;
        }

    }

    this.filter = {
        gender_m: false,
        gender_w: false,
        age_35: false,
        age_35_40: false,
        age_40_45: false,
        age_45: false,
    };

    this.filter_method = {
        gender_m: function (item) {
            return item.gender == 'male';
        },
        gender_w: function (item) {
            return item.gender == 'female';
        },
        age_35: function (item) {
            return item.dob.age <= 35;
        },

        age_35_40: function (item) {
            return item.dob.age > 35 && item.dob.age <= 40;
        },
        age_40_45: function (item) {
            return item.dob.age > 40 && item.dob.age <= 45;
        },
        age_45: function (item) {
            return item.dob.age > 45;
        },
    }

    this.all = function () {

        this.data = [...this._data];
        //this.data = this._data;
        if (this.search.length){
            this_ = this;
            this.data = this.data.filter(function (item) {

                if(item.email.indexOf(this_.search) != -1) return true;
                if(item.name.first.indexOf(this_.search) != -1) return true;
                if(item.name.last.indexOf(this_.search) != -1) return true;
                if(item.phone.indexOf(this_.search) != -1) return true;
                return false;
            });
        }

        for(elem in this.compare)
        {
            if(this.compare[elem]){
                this_ = this;
                this.data = this.data.sort(function(a,b){
                    for(elem in this_.compare)
                    {
                        if(this_.compare[elem]){
                            result = (this_.comparison_methods[elem])(a,b);
                            if (result != 0 ) return result;
                        }
                    }
                    return  0;
                });
                break;
            }
        }

        for(elem in this.filter)
        {
            if(this.filter[elem]){
                this_ = this;
                this.data = this.data.filter(function(item){
                    for(elem in this_.filter)
                    {
                        if(this_.filter[elem]){
                            result = (this_.filter_method[elem])(item);
                            if (result == false ) return result;
                        }
                    }
                    return  true;
                });
                break;
            }
        }

        return this.data;
    }
}


let Template = function () {
    this.render = function () {
        result = this.template;
        for (vars in this) {
            if (vars == 'template') continue;
            while (result.indexOf('{' + vars + '}') != -1) {
                result = result.replace('{' + vars + '}', this[vars]);
            }

        }
        return result;
    }
}


let App = function (elem, data) {

    this.app_selector = '#' + elem;
    this.tempates = {};

    this.selectors = data.selectors;
    this.userData = new UserData(users_count);
    this.setTemplate = function (name, template) {
        this.tempates[name] = new Template();
        this.tempates[name].template = template;
    }

    for (template in data.templates) {
        this.setTemplate(template, data.templates[template])
    }

    Object.defineProperty(this, "search", {
        get: function() {
            return this.userData.search;
        },
        set(v) {
            this.userData.search = v;
            this.renderContent();
            this.renderStatistic();
        }
    });


    Object.defineProperty(this, "compare", {
        get: function() {
            return this.userData.compare;
        },
        set(v) {
            for(elem in v)
            {
                if(this.userData.compare.hasOwnProperty(elem)){
                    this.userData.compare[elem] = v[elem];
                }
            }

            this.renderContent();
            this.renderStatistic();
        }
    });

    Object.defineProperty(this, "filter", {
        get: function() {
            return this.userData.filter;
        },
        set(v) {
            for(elem in v)
            {
                if(this.userData.filter.hasOwnProperty(elem)){
                    this.userData.filter[elem] = v[elem];
                }
            }

            this.renderContent();
            this.renderStatistic();
        }
    });

    this.renderContent = function () {
        data = this.userData.all();
        card_template = this.tempates.card;
        content = '';
        for (elem in data) {
            card_template.image = data[elem].picture.large;
            card_template.name = data[elem].name.title + ' ' + data[elem].name.first + ' ' + data[elem].name.last;
            card_template.phone = data[elem].phone;
            card_template.email = data[elem].email;
            card_template.addr = data[elem].location.state + ' ' + data[elem].location.city + ' ' + data[elem].location.street.name + ' ' + data[elem].location.street.number;
            content += card_template.render();
        }
        $(this.app_selector + ' ' + this.selectors.content).html(content);
    }

    this.renderStatistic = function () {
        statistic_template = this.tempates.statistic;
        statistic = this.userData.statistic();

        statistic_template.users = statistic.users;
        statistic_template.women = statistic.women;
        statistic_template.men = statistic.men;
        statistic_template.gender_by_qty = statistic.gender_by_qty;
        statistic_template.statistic = '';
        statistic_template.telegram = statistic.telegeram;


        statistic_template.statistic = '<dl>';
        for (elem in statistic.statistic) {
            statistic_template.statistic += '<dt>' + elem + '</dt>' + '<dd>' + statistic.statistic[elem] + '</dd>';
        }
        statistic_template.statistic += '</dl>';

        $(this.app_selector + ' ' + this.selectors.statistic).html(statistic_template.render());
    }

}


